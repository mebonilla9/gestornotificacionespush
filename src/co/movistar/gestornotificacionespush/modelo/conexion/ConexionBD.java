/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.movistar.gestornotificacionespush.modelo.conexion;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import co.movistar.gestornotificacionespush.negocio.constantes.EMensajes;
import co.movistar.gestornotificacionespush.negocio.excepciones.GestorNotificacionesPushException;

/**
 *
 * @author Fabian
 */
public class ConexionBD {

    public static Connection conectar() throws GestorNotificacionesPushException {
        try {
            Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
            //Connection cnn = DriverManager.getConnection("jdbc:sqlserver://Z1VMCOMASBD01\\PLANEACION;databaseName=App_planecom", "usr_gestion", "Cambio.1");
            Connection cnn = DriverManager.getConnection("jdbc:sqlserver://localhost;database=App_Pruebas", "sa", "Rinnegan1988!");
            cnn.setAutoCommit(false);
            return cnn;
        } catch (SQLException | ClassNotFoundException ex) {
            throw new GestorNotificacionesPushException(EMensajes.ERROR_CONEXION_BD);
        }
    }

    public static void desconectar(Connection cnn) {
        desconectar(cnn, null);
    }

    public static void desconectar(PreparedStatement ps) {
        desconectar(null, ps);

    }

    public static void desconectar(Connection cnn, PreparedStatement ps) {
        try {
            if (ps != null) {
                ps.close();
            }
            if (cnn != null) {
                cnn.close();
            }
        } catch (SQLException ex) {
            Logger.getLogger(ConexionBD.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public static void rollback(Connection cnn) {
        try {
            cnn.rollback();
        } catch (SQLException ex) {
            Logger.getLogger(ConexionBD.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

}
