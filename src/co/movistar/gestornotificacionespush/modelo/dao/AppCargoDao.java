package co.movistar.gestornotificacionespush.modelo.dao;

import co.movistar.gestornotificacionespush.modelo.dao.crud.AppCargoCrud;
import java.sql.Connection;

/**
 *
 * @author Manuel Ernesto Bonilla Muñoz <mebonilla9@gmail.com>
 */
public class AppCargoDao extends AppCargoCrud {

    public AppCargoDao(Connection cnn) {
        super(cnn);
    }
}
