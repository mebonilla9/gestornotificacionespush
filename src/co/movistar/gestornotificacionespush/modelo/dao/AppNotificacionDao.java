package co.movistar.gestornotificacionespush.modelo.dao;

import co.movistar.gestornotificacionespush.modelo.conexion.ConexionBD;
import java.sql.Connection;
import co.movistar.gestornotificacionespush.modelo.dao.crud.AppNotificacionCrud;
import static co.movistar.gestornotificacionespush.modelo.dao.crud.AppNotificacionCrud.getAppNotificacion;
import co.movistar.gestornotificacionespush.modelo.vo.AppNotificacion;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class AppNotificacionDao extends AppNotificacionCrud {

    private final Integer activo = 1;
    private final Integer nivel = 1;

    public AppNotificacionDao(Connection cnn) {
        super(cnn);
    }

    public AppNotificacion consultarActivos() throws SQLException {
        PreparedStatement sentencia = null;
        AppNotificacion notificacion = null;
        try {
            String sql = "select top 1* from APP_NOTIFICACION where Activo = ? and Nivel = ?";
            sentencia = cnn.prepareStatement(sql);
            sentencia.setLong(1, activo);
            sentencia.setLong(2, nivel);
            ResultSet rs = sentencia.executeQuery();
            if (rs.next()) {
                notificacion = getAppNotificacion(rs);
            }
        } finally {
            ConexionBD.desconectar(sentencia);
        }
        return notificacion;
    }
}
