package co.movistar.gestornotificacionespush.modelo.dao.crud;

import co.movistar.gestornotificacionespush.modelo.conexion.ConexionBD;
import co.movistar.gestornotificacionespush.modelo.vo.AppCargo;
import co.movistar.gestornotificacionespush.modelo.vo.AppEstados;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.ArrayList;
import java.sql.Statement;
import java.util.Map;

/**
 *
 * @author Manuel Ernesto Bonilla Muñoz <mebonilla9@gmail.com>
 */
public class AppCargoCrud implements IGenericoDAO<AppCargo> {

    protected final int ID = 1;
    protected Connection cnn;

    public AppCargoCrud(Connection cnn) {
        this.cnn = cnn;
    }

    @Override
    public void insertar(AppCargo appCargo) throws SQLException {
        PreparedStatement sentencia = null;
        try {
            int i = 1;
            String sql = "insert into APP_CARGO(ID_ESTADO,NOM_CARGO) values (?,?)";
            sentencia = cnn.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
            sentencia.setObject(i++, appCargo.getEstado().getIdEstado());
            sentencia.setObject(i++, appCargo.getNomCargo());

            sentencia.executeUpdate();
            ResultSet rs = sentencia.getGeneratedKeys();
            if (rs.next()) {
                appCargo.setIdCargo(rs.getLong(ID));
            }
        } finally {
            ConexionBD.desconectar(sentencia);
        }
    }

    @Override
    public void editar(AppCargo appCargo) throws SQLException {
        PreparedStatement sentencia = null;
        try {
            int i = 1;
            String sql = "update APP_CARGO set ID_ESTADO=?,NOM_CARGO=? where ID_CARGO=?";
            sentencia = cnn.prepareStatement(sql);
            sentencia.setObject(i++, appCargo.getEstado().getIdEstado());
            sentencia.setObject(i++, appCargo.getNomCargo());
            sentencia.setObject(i++, appCargo.getIdCargo());

            sentencia.executeUpdate();
        } finally {
            ConexionBD.desconectar(sentencia);
        }
    }

    @Override
    public List<AppCargo> consultar() throws SQLException {
        PreparedStatement sentencia = null;
        List<AppCargo> lista = new ArrayList<>();
        try {

            String sql = "select * from APP_CARGO";
            sentencia = cnn.prepareStatement(sql);
            ResultSet rs = sentencia.executeQuery();
            while (rs.next()) {
                lista.add(getAppCargo(rs));
            }
        } finally {
            ConexionBD.desconectar(sentencia);
        }
        return lista;

    }

    @Override
    public AppCargo consultar(Long id) throws SQLException {
        PreparedStatement sentencia = null;
        AppCargo obj = null;
        try {

            String sql = "select * from APP_CARGO where ID_CARGO=?";
            sentencia = cnn.prepareStatement(sql);
            sentencia.setLong(1, id);
            ResultSet rs = sentencia.executeQuery();
            if (rs.next()) {
                obj = getAppCargo(rs);
            }
        } finally {
            ConexionBD.desconectar(sentencia);
        }
        return obj;
    }

    public static AppCargo getAppCargo(ResultSet rs) throws SQLException {
        AppCargo appCargo = new AppCargo();
        appCargo.setIdCargo(rs.getLong("ID_CARGO"));
        appCargo.setEstado(new AppEstados(rs.getLong("ID_ESTADO")));
        appCargo.setNomCargo(rs.getString("NOM_CARGO"));
        return appCargo;
    }

    public static AppCargo getAppCargo(ResultSet rs, Map<String, Integer> columnas) throws SQLException {
        AppCargo appCargo = new AppCargo();
        Integer columna = columnas.get("APP_CARGO_ID_CARGO");
        if (columna != null) {
            appCargo.setIdCargo(rs.getLong(columna));
        }
        columna = columnas.get("APP_CARGO_ID_ESTADO");
        if (columna != null) {
            appCargo.setEstado(new AppEstados(rs.getLong(columna)));
        }
        columna = columnas.get("APP_CARGO_NOM_CARGO");
        if (columna != null) {
            appCargo.setNomCargo(rs.getString(columna));
        }
        return appCargo;
    }

}
