package co.movistar.gestornotificacionespush.modelo.dao.crud;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.ArrayList;
import java.sql.Statement;
import java.util.Map;
import co.movistar.gestornotificacionespush.modelo.conexion.ConexionBD;
import co.movistar.gestornotificacionespush.modelo.vo.AppCargo;
import co.movistar.gestornotificacionespush.modelo.vo.AppNotificacion;

public class AppNotificacionCrud implements IGenericoDAO<AppNotificacion> {

    protected final int ID = 1;
    protected Connection cnn;

    public AppNotificacionCrud(Connection cnn) {
        this.cnn = cnn;
    }

    @Override
    public void insertar(AppNotificacion appNotificacion) throws SQLException {
        PreparedStatement sentencia = null;
        try {
            int i = 1;
            String sql = "insert into APP_NOTIFICACION(Notificacion,Activo) values (?,?)";
            sentencia = cnn.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
            sentencia.setObject(i++, appNotificacion.getNotificacion());
            sentencia.setObject(i++, appNotificacion.getActivo());
            sentencia.executeUpdate();
            ResultSet rs = sentencia.getGeneratedKeys();
            if (rs.next()) {
                appNotificacion.setIdNotificacion(rs.getLong(ID));
            }
        } finally {
            ConexionBD.desconectar(sentencia);
        }
    }

    @Override
    public void editar(AppNotificacion appNotificacion) throws SQLException {
        PreparedStatement sentencia = null;
        try {
            int i = 1;
            String sql = "update APP_NOTIFICACION set Notificacion=?,Activo=? where Id_notificacion=?";
            sentencia = cnn.prepareStatement(sql);
            sentencia.setObject(i++, appNotificacion.getNotificacion());
            sentencia.setObject(i++, appNotificacion.getActivo());
            sentencia.setObject(i++, appNotificacion.getIdNotificacion());
            sentencia.executeUpdate();
        } finally {
            ConexionBD.desconectar(sentencia);
        }
    }

    @Override
    public List<AppNotificacion> consultar() throws SQLException {
        PreparedStatement sentencia = null;
        List<AppNotificacion> lista = new ArrayList<>();
        try {
            String sql = "select * from APP_NOTIFICACION";
            sentencia = cnn.prepareStatement(sql);
            ResultSet rs = sentencia.executeQuery();
            while (rs.next()) {
                lista.add(getAppNotificacion(rs));
            }
        } finally {
            ConexionBD.desconectar(sentencia);
        }
        return lista;

    }

    @Override
    public AppNotificacion consultar(Long id) throws SQLException {
        PreparedStatement sentencia = null;
        AppNotificacion obj = null;
        try {
            String sql = "select * from APP_NOTIFICACION where Id_notificacion=?";
            sentencia = cnn.prepareStatement(sql);
            sentencia.setLong(1, id);
            ResultSet rs = sentencia.executeQuery();
            if (rs.next()) {
                obj = getAppNotificacion(rs);
            }
        } finally {
            ConexionBD.desconectar(sentencia);
        }
        return obj;
    }

    public static AppNotificacion getAppNotificacion(ResultSet rs) throws SQLException {
        AppNotificacion appNotificacion = new AppNotificacion();
        appNotificacion.setIdNotificacion(rs.getLong("Id_notificacion"));
        appNotificacion.setNotificacion(rs.getString("Notificacion"));
        appNotificacion.setActivo(rs.getLong("Activo"));
        appNotificacion.setCargo(new AppCargo(rs.getLong("id_cargo")));
        appNotificacion.setNivel(rs.getLong("Nivel"));
        return appNotificacion;
    }

    public static AppNotificacion getAppNotificacion(ResultSet rs, Map<String, Integer> columnas) throws SQLException {
        AppNotificacion appNotificacion = new AppNotificacion();
        Integer columna = columnas.get("APP_NOTIFICACION_Id_notificacion");
        if (columna != null) {
            appNotificacion.setIdNotificacion(rs.getLong(columna));
        }
        columna = columnas.get("APP_NOTIFICACION_Notificacion");
        if (columna != null) {
            appNotificacion.setNotificacion(rs.getString(columna));
        }
        columna = columnas.get("APP_NOTIFICACION_Activo");
        if (columna != null) {
            appNotificacion.setActivo(rs.getLong(columna));
        }
        columna = columnas.get("APP_NOTIFICACION_id_cargo");
        if (columna != null) {
            appNotificacion.setCargo(new AppCargo(rs.getLong("id_cargo")));
        }
        columna = columnas.get("APP_NOTIFICACION_Nivel");
        if (columna != null) {
            appNotificacion.setNivel(rs.getLong("Nivel"));
        }
        return appNotificacion;
    }

}
