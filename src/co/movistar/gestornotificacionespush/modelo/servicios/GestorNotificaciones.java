/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.movistar.gestornotificacionespush.modelo.servicios;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;

/**
 *
 * @author Lord_Nightmare
 */
public final class GestorNotificaciones {

    private static final String API_SERVER_KEY = "AAAAk3PkB00:APA91bGdLK6dLXvH6bLWVy_9py2FimWqK8FsowjcdGZW_vSW66gj26pmh8bQhVgZLkv1Jq_WiLyc-J1vNBKRpEXjsXNV5JsJBdfaThAON02K0J-m0f_yyRr54bEYaOR4nxKYKTBp-5XQ";
    private static final String URL_SERVIDOR = "https://fcm.googleapis.com/fcm/send";
    private static final String HTTP_METHOD = "POST";

    public static String enviarNotificacion(String para, String titulo, String mensaje) {
        String responseData = "";
        try {
            // Asignacion del objeto que representa la url del servidor de
            // notificaciones
            URL rutaServidor = new URL(URL_SERVIDOR);
            // Apertura de la conexion a traves de un objeto de tipo
            // <code>HttpUrlConnection</code>
            HttpURLConnection conexion = (HttpURLConnection) rutaServidor.openConnection();
            // Se establece el metodo http para el envio de la peticion
            conexion.setRequestMethod(HTTP_METHOD);
            // Se habilita la conexion para la recepcion de valores de respuesta
            conexion.setDoInput(true);
            // Se habilita la conexion para el envio de valores por parametros
            conexion.setDoOutput(true);
            // Se agrega el header de la peticion de tipo autenticacion
            conexion.setRequestProperty("Authorization", "key=" + API_SERVER_KEY);
            // Se agrega el header del tipo de informacion que envia y recibe
            // la peticion
            conexion.setRequestProperty("Content-Type", "application/json");
            OutputStreamWriter osw = new OutputStreamWriter(conexion.getOutputStream());
            JsonObject payload = new JsonObject();
            payload.addProperty("to", para);
            JsonObject data = new JsonObject();
            data.addProperty("title", titulo);
            data.addProperty("body", mensaje);
            payload.add("notification", data);
            System.out.println(new Gson().toJson(payload));
            osw.write(new Gson().toJson(payload));
            osw.flush();
            osw.close();

            BufferedReader br = new BufferedReader(new InputStreamReader(conexion.getInputStream()));
            String linea;
            StringBuilder resultado = new StringBuilder();
            while ((linea = br.readLine()) != null) {
                resultado.append(linea);
            }
            responseData = resultado.toString();
        } catch (IOException e) {
            e.printStackTrace(System.err);
        }
        return responseData;
    }
 
}
