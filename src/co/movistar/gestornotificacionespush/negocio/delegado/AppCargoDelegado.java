/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.movistar.gestornotificacionespush.negocio.delegado;

import co.movistar.gestornotificacionespush.modelo.dao.AppCargoDao;
import co.movistar.gestornotificacionespush.modelo.dto.AuditoriaDTO;
import co.movistar.gestornotificacionespush.modelo.vo.AppCargo;
import co.movistar.gestornotificacionespush.negocio.excepciones.GestorNotificacionesPushException;
import java.sql.Connection;

/**
 *
 * @author Manuel Ernesto Bonilla Muñoz <mebonilla9@gmail.com>
 */
public class AppCargoDelegado extends GenericoDelegado<AppCargo> {

    private final AppCargoDao appCargoDao;

    public AppCargoDelegado(Connection cnn, AuditoriaDTO auditoria) throws GestorNotificacionesPushException {
        super(cnn, auditoria);
        appCargoDao = new AppCargoDao(cnn);
        genericoDAO = appCargoDao;
    }

}
