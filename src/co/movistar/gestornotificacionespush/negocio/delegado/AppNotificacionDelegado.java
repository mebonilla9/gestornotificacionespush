/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.movistar.gestornotificacionespush.negocio.delegado;

import co.movistar.gestornotificacionespush.modelo.conexion.ConexionBD;
import co.movistar.gestornotificacionespush.negocio.excepciones.GestorNotificacionesPushException;
import co.movistar.gestornotificacionespush.modelo.dao.AppNotificacionDao;
import co.movistar.gestornotificacionespush.modelo.dto.AuditoriaDTO;
import co.movistar.gestornotificacionespush.modelo.vo.AppNotificacion;
import co.movistar.gestornotificacionespush.negocio.constantes.EMensajes;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

/**
 *
 * @author Fabian
 */
public class AppNotificacionDelegado extends GenericoDelegado<AppNotificacion> {

    private final AppNotificacionDao appNotificacionDao;

    public AppNotificacionDelegado(Connection cnn, AuditoriaDTO auditoria) throws GestorNotificacionesPushException {
        super(cnn, auditoria);
        appNotificacionDao = new AppNotificacionDao(cnn);
        genericoDAO = appNotificacionDao;
    }

    public AppNotificacion consultarActivos() throws GestorNotificacionesPushException {
        try {
            return appNotificacionDao.consultarActivos();
        } catch (SQLException ex) {
            ex.printStackTrace(System.err);
            ConexionBD.rollback(cnn);
            throw new GestorNotificacionesPushException(EMensajes.ERROR_CONSULTAR);
        }
    }
}
