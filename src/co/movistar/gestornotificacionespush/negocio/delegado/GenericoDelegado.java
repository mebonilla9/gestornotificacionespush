/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.movistar.gestornotificacionespush.negocio.delegado;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;
import co.movistar.gestornotificacionespush.negocio.constantes.EMensajes;
import co.movistar.gestornotificacionespush.negocio.excepciones.GestorNotificacionesPushException;
import co.movistar.gestornotificacionespush.modelo.conexion.ConexionBD;
import co.movistar.gestornotificacionespush.modelo.dao.crud.IGenericoDAO;
import co.movistar.gestornotificacionespush.modelo.dto.AuditoriaDTO;

/**
 *
 * @author Manuel Ernesto Bonilla Muñoz <mebonilla9@gmail.com>
 */
public abstract class GenericoDelegado<T> {

    protected Connection cnn;
    protected IGenericoDAO genericoDAO;
    protected boolean confirmar = true;

    public GenericoDelegado(Connection cnn, AuditoriaDTO auditoriaDTO) throws GestorNotificacionesPushException {
        this.cnn = cnn;
    }

    public void insertar(T entidad) throws GestorNotificacionesPushException {
        try {
            genericoDAO.insertar(entidad);
        } catch (SQLException ex) {
            ConexionBD.rollback(cnn);
            throw new GestorNotificacionesPushException(EMensajes.ERROR_INSERTAR);
        }
    }

    public void editar(T entidad) throws GestorNotificacionesPushException {
        try {
            genericoDAO.editar(entidad);

        } catch (SQLException ex) {
            ConexionBD.rollback(cnn);
            throw new GestorNotificacionesPushException(EMensajes.ERROR_MODIFICAR);
        }
    }

    public List<T> consultar() throws GestorNotificacionesPushException {
        try {
            return genericoDAO.consultar();
        } catch (SQLException ex) {
            ConexionBD.rollback(cnn);
            throw new GestorNotificacionesPushException(EMensajes.ERROR_CONSULTAR);
        }
    }

    public T consultar(long id) throws GestorNotificacionesPushException {
        try {
            return (T) genericoDAO.consultar(id);
        } catch (SQLException ex) {
            ConexionBD.rollback(cnn);
            throw new GestorNotificacionesPushException(EMensajes.ERROR_CONSULTAR);
        }
    }

}
