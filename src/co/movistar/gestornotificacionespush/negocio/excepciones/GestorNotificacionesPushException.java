/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.movistar.gestornotificacionespush.negocio.excepciones;

import co.movistar.gestornotificacionespush.negocio.constantes.EMensajes;

/**
 *
 * @author Fabian
 */
public class GestorNotificacionesPushException extends Exception {

    private int codigo;
    private String mensaje;
    private Object datos;

    public GestorNotificacionesPushException(EMensajes mensaje) {
        this.codigo = mensaje.getCodigo();
        this.mensaje = mensaje.getDescripcion();
    }

    public GestorNotificacionesPushException(EMensajes mensaje, Object datos) {
        this.codigo = mensaje.getCodigo();
        this.mensaje = mensaje.getDescripcion();
        this.datos = datos;
    }

    public int getCodigo() {
        return codigo;
    }

    public void setCodigo(int codigo) {
        this.codigo = codigo;
    }

    public String getMensaje() {
        return mensaje;
    }

    public void setMensaje(String mensaje) {
        this.mensaje = mensaje;
    }

    public Object getDatos() {
        return datos;
    }

    public void setDatos(Object datos) {
        this.datos = datos;
    }

}
