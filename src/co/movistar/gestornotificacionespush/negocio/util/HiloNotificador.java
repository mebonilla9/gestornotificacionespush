/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.movistar.gestornotificacionespush.negocio.util;

import co.movistar.gestornotificacionespush.modelo.conexion.ConexionBD;
import co.movistar.gestornotificacionespush.modelo.servicios.GestorNotificaciones;
import co.movistar.gestornotificacionespush.modelo.vo.AppNotificacion;
import co.movistar.gestornotificacionespush.negocio.constantes.EMensajes;
import co.movistar.gestornotificacionespush.negocio.delegado.AppCargoDelegado;
import co.movistar.gestornotificacionespush.negocio.delegado.AppNotificacionDelegado;
import co.movistar.gestornotificacionespush.negocio.excepciones.GestorNotificacionesPushException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;
import javax.swing.JTextPane;

/**
 *
 * @author Lord_Nightmare
 */
public class HiloNotificador extends Thread {

    private final String topico = "/topics/";
    private String para = topico;
    private final String temaGlobal = "notificacionMovistar";
    private final String titulo = "Nueva Información disponible";
    private final JTextPane etiquetaMensaje;
    private boolean enEjecucion;

    public HiloNotificador(JTextPane etiquetaMensaje) {
        this.etiquetaMensaje = etiquetaMensaje;
        this.enEjecucion = true;
    }

    @Override
    public void run() {
        try {
            while (true) {
                if (isEnEjecucion()) {
                    String mensaje = this.generarMensajeNotificacion();
                    if (!mensaje.equals("")) {
                        PanelTextoUtil.actualizarPanelLog(this.etiquetaMensaje, GestorNotificaciones.enviarNotificacion(para, titulo, mensaje) + " \n " + mensaje);
                        actualizarTablaNotificaciones();
                    }
                }
                Thread.sleep(TimeUnit.MINUTES.toMillis(1));
            }
        } catch (InterruptedException e) {
            e.printStackTrace(System.err);
        }
    }

    private AppNotificacion consultarInformacionActivos() throws GestorNotificacionesPushException {
        AppNotificacion notificacion = null;
        Connection cnn = null;
        try {
            cnn = ConexionBD.conectar();
            notificacion = new AppNotificacionDelegado(cnn, null).consultarActivos();
        } finally {
            ConexionBD.desconectar(cnn);
        }
        return notificacion;
    }

    private String generarMensajeNotificacion() {
        String mensaje = "";
        para = topico;
        try {
            AppNotificacion notificacion = this.consultarInformacionActivos();
            if(notificacion == null){
                throw new GestorNotificacionesPushException(EMensajes.ERROR_CONSULTAR);
            }
            if (notificacion != null) {
                mensaje = notificacion.getNotificacion();
            }
            if (notificacion.getCargo().getIdCargo() != 0) {
                Connection cnn = ConexionBD.conectar();
                notificacion.setCargo(new AppCargoDelegado(cnn, null).consultar(notificacion.getCargo().getIdCargo()));
                para += notificacion.getCargo().getNomCargo();
            } else {
                para += temaGlobal;
            }
        } catch (GestorNotificacionesPushException e) {
            e.printStackTrace();
            mensaje = "";
        }
        return mensaje;
    }

    /**
     * @return the enEjecucion
     */
    public boolean isEnEjecucion() {
        return enEjecucion;
    }

    /**
     * @param enEjecucion the enEjecucion to set
     */
    public void setEnEjecucion(boolean enEjecucion) {
        this.enEjecucion = enEjecucion;
    }

    private void actualizarTablaNotificaciones() {
        Connection cnn = null;
        try {
            cnn = ConexionBD.conectar();
            AppNotificacion notificacion = this.consultarInformacionActivos();
            notificacion.setActivo(Long.parseLong("0"));
            new AppNotificacionDelegado(cnn, null).editar(notificacion);
            cnn.commit();
        } catch (SQLException | GestorNotificacionesPushException e) {

        } finally {
            ConexionBD.desconectar(cnn);
        }
    }
}
