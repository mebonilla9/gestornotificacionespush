/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.movistar.gestornotificacionespush.negocio.util;

import javax.swing.JTextPane;

/**
 *
 * @author Lord_Nightmare
 */
public final class PanelTextoUtil {
    
    public static void actualizarPanelLog(JTextPane panel, String mensaje){
        StringBuilder texto = new StringBuilder(panel.getText());
        texto.append("\n");
        texto.append(mensaje);
        panel.setText("");
        panel.setText(texto.toString());
    }
    
}
